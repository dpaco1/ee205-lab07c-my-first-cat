///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// Hello World Program using namespace std and printing newline with object endl.
///
/// @file    hello1.cpp
///
/// Compile: 
/// make
/// make test
/// make debug
///          
/// author  David Paco <dpaco@hawaii.edu>
/// date    2_3_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

int main() {
   
cout << "Hello World!" << endl;

}
